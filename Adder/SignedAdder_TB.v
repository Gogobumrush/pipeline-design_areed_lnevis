module SignedAdder_TB();
    
    reg signed[15:0] A, B;
    wire signed[15:0] out;
    
    SignedAdder uut(A,B,out);
    
    initial begin
        $dumpfile("Test.vcd");
        $dumpvars(0,A);
        $dumpvars(1,B);
        $dumpvars(2, out);
        A = 10;
        B = -1;
        #1;
        A=15;
        B=-5;
        #1;
        A = 100;
        B = 10;
        #1;
        A = 30;
        B = -50;
        #1
        A = 35;
        B = -35;
        
        
        #1 $finish;
    
    end
    
endmodule