module Pipeline_TB();
    
    reg clk;
    Pipeline uut(clk);
    integer i;
    initial begin
        $dumpfile("pipeline.vcd");
        $dumpvars(0,Pipeline_TB);
        clk = 0;
        #1
        clk = 1;
        #1;
        clk = 0;
        #1;
        clk = 1;
        #1;
        clk = 0;
        #1;
        clk = 1;
        #1
        clk = 0;
        #1
        for( i = 0; i < 100; i ++ ) begin
            clk = 1;
            #1;
            clk = 0;
            #1;
        end
        #1 $finish;
    end
endmodule