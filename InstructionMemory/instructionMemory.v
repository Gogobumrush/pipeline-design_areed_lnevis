module instructionMemory( address, instruction );
    
    input [15:0] address;
    output reg [15:0] instruction;

    reg [7:0] dataMemory [255:0];
    
    integer i;
    initial begin
    
        // ADD R1, R2  
        dataMemory[0] = 16'b0000_0001;
        dataMemory[1] = 16'b0010_1111;
        
        // SUB R1, R2
        dataMemory[2] = 16'b0000_0001;
        dataMemory[3] = 16'b0010_1110;
        
        // OR R3, R4
        dataMemory[4] = 16'b0000_0011;
        dataMemory[5] = 16'b0100_1100;
        
        // AND R3, R2
        dataMemory[6] = 16'b0000_0011;
        dataMemory[7] = 16'b0010_1101;
        
        //MUL R5, R6
        dataMemory[8] = 16'b0000_0101;
        dataMemory[9] = 16'b0110_0001;
        
        // DIV R1, R5
        dataMemory[10] = 16'b0000_0001;
        dataMemory[11] = 16'b0101_0010;
        
        // SUB R0, R0
        dataMemory[12] = 16'b0000_0000;
        dataMemory[13] = 16'b0000_1110;
        
        // SLL R4, 3
        dataMemory[14] = 16'b0000_0100;
        dataMemory[15] = 16'b0011_1010;
        
        // SLR R4, 2
        dataMemory[16] = 16'b0000_0100;
        dataMemory[17] = 16'b0010_1011;
        
        // ROR R6, 3
        dataMemory[18] = 16'b0000_0110;
        dataMemory[19] = 16'b0011_1001;
        
        // ROL R6, 2
        dataMemory[20] = 16'b0000_0110;
        dataMemory[21] = 16'b0010_1000;
        
        // BEQ R7,4 
        dataMemory[22] = 16'b0110_0111;
        dataMemory[23] = 16'b0000_0100;
        
        // ADD R11, R1
        dataMemory[24] = 16'b0000_1011;
        dataMemory[25] = 16'b0001_1111;
        
        // BLT R7, 5
        dataMemory[26] = 16'b0100_0111;
        dataMemory[27] = 16'b0000_0101;
        
        // ADD R11, R2
        dataMemory[28] = 16'b0000_1011;
        dataMemory[29] = 16'b0010_1111;
        
        // BGT R7, 2
        dataMemory[30] = 16'b0101_0111;
        dataMemory[31] = 16'b0000_0010;
        
        // ADD R1, R1,
        dataMemory[32] = 16'b0000_0001;
        dataMemory[33] = 16'b0001_1111;
        
        // ADD R1, R1,
        dataMemory[34] = 16'b0000_0001;
        dataMemory[35] = 16'b0001_1111;
        
        // LW R8, 0(R9)
        dataMemory[36] = 16'b1000_1000;
        dataMemory[37] = 16'b1001_0000;
        
        // ADD R8, R8
        dataMemory[38] = 16'b0000_1000;
        dataMemory[39] = 16'b1000_1111;
        
        // SW R8, 2 (R9)
        dataMemory[40] = 16'b1011_1000;
        dataMemory[41] = 16'b1001_0010;
        
        // LW R10, 2 (R9)
        dataMemory[42] = 16'b1000_1010;
        dataMemory[43] = 16'b1001_0010;
        
        // ADD R12, R12
        dataMemory[44] = 16'b0000_1100;
        dataMemory[45] = 16'b1100_1111;
        
        // SUB R13, R13
        dataMemory[46] = 16'b0000_1101;
        dataMemory[47] = 16'b1101_1110;
        
        // ADD R12, R12
        dataMemory[48] = 16'b0000_1100;
        dataMemory[49] = 16'b1100_1111;
        
        //STOP! YOU HAVE VIOLATED THE LAW! PAY THE COURT A FINE OR SERVE YOUR SENTENCE!
        dataMemory[50] = 16'b1111_1111;
        dataMemory[51] = 16'b1111_1111;
        
        for( i = 50; i < 255; i++ ) begin
            dataMemory[i] = 16'b11101111;
        end
    end
    
    always@(address) begin
        instruction[15:8] = dataMemory[address];
        instruction[7:0]  = dataMemory[address+1];
    end


endmodule