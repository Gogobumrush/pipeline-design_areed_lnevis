# README #


## The objective of this project is to design and simulate the datapath and control unit for a pipelined system, which can execute assembly instructions.  ##

### We are given 16 registers: R0 - R15 to work with our data that are initialized to given values. ###

https://bitbucket.org/Gogobumrush/pipeline-design_areed_lnevis/src/master/InitialRegisters.PNG


### We are given a set of assembly instructions that we are allowed to use for processing the given test assembly code. This code is processed using Verilog. ###

https://bitbucket.org/Gogobumrush/pipeline-design_areed_lnevis/src/master/assembly_Instructions.PNG

### Given a set of instructions that must process through our pipeline accounting for possible forwarding, branching, and potential hazards. ###

https://bitbucket.org/Gogobumrush/pipeline-design_areed_lnevis/src/master/InstructionSetOne.PNG

### A Diagram was designed to track the different components of our design ###

https://bitbucket.org/Gogobumrush/pipeline-design_areed_lnevis/src/master/Pipeline_Diagram.jpg

* The Forwarding Controller that ensures that registers used in consecutive instructions store the correect values.
* Data memory to keep track and store our registers
* Controller to send the appropriate commands based on instructions given.
* Arithmetic Logical Unit (ALU) to handle mathematical instructions.
* This is broken out into stages based on what cycle we are in.