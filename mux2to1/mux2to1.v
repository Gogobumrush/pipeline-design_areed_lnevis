module mux2to1(
				input    [15:0] d0, d1, 
				input s,
				output reg [15:0] y
			   );
always@(*)
begin
   if (s)  y = d1;
   else   y = d0;
end

endmodule

