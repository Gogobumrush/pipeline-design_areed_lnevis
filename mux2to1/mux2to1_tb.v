
`timescale 1ns / 1ns
module mux2to1_tb;

reg       [15:0] d0, d1;
reg 			 s;
wire      [15:0] y;

mux2to1   uut (.d1(d1), .d0(d0), .s(s), .y(y));




initial begin
 $dumpfile("mux2to1_tb.vcd");
     $dumpvars(0,mux2to1_tb);
          d1 = 16'h0040;  d0 = 16'h0020;  s = 0;
  #10  
  s = 0;
   
  #10  
   s = 1;

  #10  
   d1 = 16'h0060;  d0 = 16'h0020;  s = 0;  
 

   
  
  
  
  #10 $stop;
  
end
initial $monitor($time, " ns, d1=%b, d0=%b, s = %b,  y=%b", d1, d0, s, y);



endmodule
