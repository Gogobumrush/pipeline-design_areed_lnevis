module Memory(address, writeData, memWrite, memRead, readData, dataAt02);
    
    //Byte addressed?
    input [15:0] address;
    input [15:0] writeData;
    input memWrite;
    input memRead;
    
    output [15:0] readData;
    
    reg [15:0] outputBuffer;
    assign readData = outputBuffer;
    
    //memory
    reg [7:0] mem [15:0];
    
    //Debug
    output wire[15:0] dataAt02;
    
    assign dataAt02[15:8] = mem[2];
    assign dataAt02[7:0] = mem[3];
    
    integer i;
    initial begin
        //packet = 'h0010 //hex example
        mem[0] = 'h2B;
        mem[1] = 'hCD;
        for (i = 2; i<65535; i=i+1 ) mem[i] <= 8'b00000000;
        outputBuffer = 0;
    end
    
    always@( posedge memRead, posedge memWrite) begin
        
        if( memRead == 1'b1 ) begin
            outputBuffer[15:8] = mem[address];
            outputBuffer[7:0] = mem[address+1];
        end
        
        if( memWrite == 1'b1 ) begin
            
            mem[address] = writeData[15:8];
            mem[address+1] = writeData[7:0];
            
        end
        
    end
    
endmodule