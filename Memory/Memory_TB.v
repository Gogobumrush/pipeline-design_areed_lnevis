module Memory_TB();
    
    //address, writeData, memWrite, memRead, readData
    reg [15:0] address;
    reg [31:0] writeData;
    reg memWrite, memRead;
    wire [31:0] readData;
    
    Memory uut(address, writeData, memWrite, memRead, readData);
    
    initial begin
        
        $dumpfile("Memory_Test.vcd");
        $dumpvars(0,address);
        $dumpvars(1, writeData);
        $dumpvars(2, memWrite);
        $dumpvars(3, memRead);
        $dumpvars(4, readData);
        
        address = 0;
        writeData = 1000;
        memWrite = 0;
        memRead = 0;
        #1;
        memWrite = 1;
        #1;
        memWrite = 0;
        #1;
        memRead = 1;
        #1;
        memRead = 0;
        #1 $finish;
    
    end
    
endmodule