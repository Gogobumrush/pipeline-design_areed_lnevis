

//forwarding_tb


module forwarding_tb;

		reg clk;
		reg [3:0] memwbReg, exmemReg, reqRegA, reqRegB, opCode, functCode;   
		wire[1:0]  muxOne, muxTwo;
					
					
					
					
forwarding uut(
				.clk(clk),
				.memwbReg(memwbReg),
				.exmemReg(exmemReg),
				.reqRegA(reqRegA),
				.reqRegB(reqRegB),
				.opCode(opCode),
				.functCode(functCode),
				.muxOne(muxOne),
				.muxTwo(muxTwo)
				);

initial begin 
	clk = 1;
	
end	
always
#10 clk = !clk;


initial begin					
	 $dumpfile("forwarding_tb.vcd");
     $dumpvars(0,forwarding_tb);
	 opCode = 4'b0000; 
	 functCode = 4'b0000; 
	 reqRegA = 4'b0010;
	 reqRegB = 4'b1000;
	 memwbReg = 4'b0000;
	 exmemReg = 4'b0000;

	 #10
	 exmemReg = 4'b0010;

	 //checking the function code if block  
	 //if any below function codes are set, then muxTwo =00 else muxTwo =01
	 #10
	 #10
	 exmemReg = 4'b1000;
	 
	 #10
	 #10
	 functCode = 4'b1011; 
	 
	 #10
	 #10
	 functCode = 4'b0000; 
     
	 #10
	 #10
	 functCode = 4'b1000; 
	 
	 #10
	 #10
	 functCode = 4'b0000; 
	 
	 #10
	 #10
	 functCode = 4'b1001; 
	 
	 #10
	 #10
	 functCode = 4'b0000; 
	 
	 
	 //testing to see if memwbReg = reqRegB
	 #10
	 #10
	 opCode= 4'b1000;
	 
	 #10
	 #10
	 
	 //this checks if both exmem and memwb are same
	 memwbReg = 4'b1000;
	 
	 #10
	 #10 
	 exmemReg = 4'b0000;
	
	
	#10
	#10
	opCode = 4'b1100;
	
	
	#10
	#10	 	 
	opCode =4'b1011;
	
	
	#10
	#10
	opCode = 4'b1100;
	

	
	 
	 
	 
	 
	 #100 $stop;
	 
	 
	 
end 
endmodule