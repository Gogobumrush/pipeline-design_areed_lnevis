module forwarding(
					input clk,
				    input [3:0] memwbReg, exmemReg, reqRegA, reqRegB, opCode, functCode, 
					output reg [1:0]  muxOne, muxTwo
				);



// MEM/WB REG, EX/MEM REG, REQUESTED REG A,REQUESTED REG B, OPCODE, FUNCTION CODE
 
initial begin
    
    muxOne = 2'b00;
    muxTwo = 2'b00;
    
end

//REG


//if op code 0 and fucnct code first 6 instructions its reg
//but if funct code first 

always@(clk)

begin
		//check if reqRegA == Wbex/mem, elseif reqRegA == Mem/WB, set default 1111 
		//check if reqRegB == Wbex/mem, elseif regReqB == Mem/WB, set default 1111

		if( opCode == 4'b1000 || opCode == 4'b1011 ) begin
            muxOne = 2'b00;
        end else begin
            if( (reqRegA == exmemReg) ) begin
                muxOne = 2'b01;
                
            end else if (reqRegA == memwbReg) begin 
                muxOne = 2'b10;
            end
            else begin
                muxOne = 2'b00;
            end
        end
		
        
		//because of immediate values possible in op2 we need to check op and function codes
			 if( (reqRegB == exmemReg)  && (opCode == 4'b0000 || opCode == 4'b0100 || opCode == 4'b0101 || opCode == 4'b0110 || opCode == 4'b1100)) begin
				//If we do normal arithmatic, shifting, rotating, branching or jumping, update the data sent to RegB.
				//this if statement sends default if Shift left/right, RoR, RoL are selected
				if( (functCode == 4'b1010) || (functCode == 4'b1011) || (functCode == 4'b1000) || (functCode == 4'b1001)) begin
					muxTwo = 2'b00;
				
				//We dont have one of the above functo codes and the op code is 0000 
				end else if (reqRegB == exmemReg)  begin
			
					muxTwo = 2'b01;
				end
		//op code was not 0000 and reqRegB = memwbreg we check other options
			end else if ( reqRegB == memwbReg && ( opCode == 4'b1000 || opCode == 4'b1011 ) )  begin
		
			muxTwo = 2'b10;
			end
		else begin
			muxTwo = 2'b00;				
			
		end 
    end
endmodule
