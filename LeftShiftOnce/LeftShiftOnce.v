module LeftShiftOnce( dataIn, dataOut );

    input [15:0] dataIn;
    output[15:0] dataOut;
    
    assign dataOut[15:1] = dataIn[14:0];
    assign dataOut[0] = 1'b0;


endmodule
