

//signed add 		  opcode 0000  functcode 1111
//signed sub  		  opcode 0000  functcode 1110

//bitwise and 		  opcode 0000  functcode 1101
//bitwise or 		  opcode 0000  functcode 1100
//signed mult 		  opcode 0000  functcode 0001
//signed div 		  opcode 0000  functcode 0010
//logical shift left  opcode 0000  functcode 1010
//logical shift right opcode 0000  functcode 1011
//rotate left 		  opcode 0000  functcode 1000
//rotate right 		  opcode 0000  functcode 1001

//values taken in :

//16 bit op1
//16 bit op2
//opcode
//functcode

//based on the design given, R0 holds the product upper half and remainder for mult/div
module ALU( input [3:0]  functcode,
			input signed [15:0] op1, op2,
			output reg cout,
			output reg signed [15:0] finalResult, R0
		   );

initial begin
    finalResult = 0;
    R0 = 0;
end
always @ (*) begin
	case(functcode)
	//add
	4'b1111: 
			begin 
				{cout, finalResult} = ({1'b0, op1} + {1'b0, op2});
			end
	//sub
	4'b1110:
			begin
				{cout,finalResult} = ({1'b0, op1} - {1'b0, op2});
			end
			
	
	//and
	4'b1101:
			finalResult = (op1 & op2);
			
	
	//or
	4'b1100:
			finalResult = (op1 | op2);
	
	//mult   product lower= op1 , product upper = r0 
	4'b0001:
			begin 
				{R0,finalResult} = ( op1 * op2);
			end
	
	//div  quotient = result r0 = remainder
	4'b0010:
			begin
			if (op1 == 0) finalResult = 0; 
				finalResult = (op1/op2);
				R0 = (op1%op2);
			end
			
	//logical shift left  functcode 1010
	4'b1010:
			begin
			finalResult =  (op1 << op2);
			end
	//logical shift right functcode 1011
	4'b1011:
			begin
			finalResult = (op1 >> op2);
			end
	//rotate left 		  functcode 1000
	4'b1000:
			begin
			finalResult = (op1 << op2 | op1>>(16-op2) );
			end
	//rotate right 		  functcode 1001
	4'b1001:
			begin
			finalResult = ((op1 >> op2) | (op1<<16-op2));
			end
    default : begin
        finalResult = 16'b0000000000000000;
    end
	endcase
end

endmodule
	