module ALU_tb();

reg [3:0] functcode;
reg [15:0] op1,op2;

wire cout;

wire [15:0] finalResult, R0;


ALU uut (
			.functcode(functcode),
			.op1(op1),
			.op2(op2),
			.cout(cout),
			.finalResult(finalResult),
			.R0(R0)
		);
		
		
///funct F add, E sub, D and, C or, 1 mult, 2 div 

initial begin
	 $dumpfile("ALU_tb.vcd");
     $dumpvars(0,ALU_tb);
	functcode = 4'b1111; op1 = 16'h0004; op2 = 16'h0002; //start with add
	
	#10
	functcode = 4'b1110; //sub
	
	#10
	op1 = 16'h0002; op2 = 16'h0004; //negative sub
	
	#10
	functcode = 4'b1101; //AND
	
	#10
	op1 = 16'h00FF; op2 = 16'h 00F0; //AND with matching bits
	
	#10
	functcode = 4'b1100; op1 = 16'h000F;//or
	
	#10 
	functcode = 4'b0001; //mult starts with 00F0 * 000F =  E10 result or  15*240 = 3,600
	
	#10 
	op1 = 16'hF000; op2 = 16'h0002; //mult -4096 * 2 = -8192 or E0000
	
	#10
	functcode = 4'b0010; //div    -4096/2 = -2048 or F8000,  no remainder
	
	#10;
	
	op1 = 16'h000D; op2 = 16'h0006;  // div 13/6 = 2 remainder 1
	
	#10;
	


	

end
initial $monitor($time, " ns, functcode=%b, op1=%d, op2 = %d, Result = %d, R0 = %d ", functcode, op1, op2, finalResult, R0);
endmodule