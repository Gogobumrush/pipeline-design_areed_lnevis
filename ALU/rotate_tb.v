module rotate_tb();


reg [3:0] op1,op2;


wire [3:0] result,resultTwo,finalResult;


rotate uut (
		
			.op1(op1),
			.op2(op2),
		
			.result(result),
			.resultTwo(resultTwo),
			.finalResult(finalResult)

		);
		
		
///funct F add, E sub, D and, C or, 1 mult, 2 div 

initial begin
	 $dumpfile("rotate_tb.vcd");
     $dumpvars(0,rotate_tb);

op1 = 4'b0010;  op2 = 4'b0001;

#10

op1 = 4'b0001; op2 = 4'b0001;

#10;

op1 = 4'b1000; op2 = 4'b0001;

#10;
end
endmodule