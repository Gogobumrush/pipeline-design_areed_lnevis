`timescale 1ns / 1ns
module mux4to1_tb;

reg      [15:0] d3, d2, d1, d0;
reg 	 [1:0 ] s;
wire     [15:0] y;


mux4to1    uut (
				.d0(d0),
				.d1(d1),
				.d2(d2),
				.d3(d3),
				.s(s),
				.y(y)
				);
				






initial begin

 $dumpfile("mux4to1_tb.vcd");
 $dumpvars(0,mux4to1_tb);
    d3 = 16'hF000; d2 = 16'h0F00; d1 = 16'h00F0;  d0 = 16'h000F;  s = 2'b00;
	#10
	
	d3 = 16'hF000; d2 = 16'h0F00; d1 = 16'h00F0;  d0 = 16'h000F;  s = 2'b01;
	#10
	
	d3 = 16'hF000; d2 = 16'h0F00; d1 = 16'h00F0;  d0 = 16'h000F;  s = 2'b10;
	#10
	
	d3 = 16'hF000; d2 = 16'h0F00; d1 = 16'h00F0;  d0 = 16'h000F;  s = 2'b11;
	#10

	
#10 $stop;
end


initial $monitor($time, "ns, d3=%d, d2=%d,d1=%d,d0=%d, s = %b,  y=%d", d3, d2, d1, d0, s, y);
endmodule
