module Registers_TB ();
    
    reg clk;
    reg [3:0] readReg1, readReg2, whichRegToWriteTo;
    reg regWrite;
    reg [15:0] writeData;
    
    wire[15:0] readData1, readData2;
    
    Registers uut(clk, readReg1, readReg2, whichRegToWriteTo, regWrite, writeData, readData1, readData2 );
    

    initial begin
        $dumpfile("Registers_Test.vcd");
        $dumpvars(0,Registers_TB);
        
        
        //Write 3 to reg 0
        //Write 15 to reg 7
        clk = 0;
        readReg1 = 0;
        readReg2 = 0;
        whichRegToWriteTo = 0;
        regWrite = 1;
        writeData = 3;
        #1;
        clk = 1;
        #1;
        clk = 0;
        writeData = 15;
        whichRegToWriteTo = 7;
        readReg2 = 7;
        #1;
        clk = 1;
        #1;
        clk = 0;
        #1 $finish;
        
    end

endmodule