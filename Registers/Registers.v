module Registers(clk, selectA, selectB, whichRegToWriteTo, regWrite, writeData, dataA, dataB );
    
    
    input clk;
    
    //Which registers are we reading
    input [3:0] selectA, selectB;
    
    //Which register do we write to
    input [3:0] whichRegToWriteTo;
    
    //Are we currently writing to a register
    input regWrite;
    
    //Data that writes to the registers
    input [15:0] writeData;
    
    //Output data
    output wire [15:0] dataA, dataB;
    
    //Registers
    reg [15:0] RN [15:0];
    
    initial begin
        RN[0]  = 0;
        RN[1]  = 16'b0000111100000000;  //0F00
        RN[2]  = 16'b0000000001010000;  //0050
        RN[3]  = 16'b1111111100001111;  //FF0F
        RN[4]  = 16'b1111000011111111;  //F0FF
        RN[5]  = 16'b0000000001000000;  //0040
        RN[6]  = 16'b0000000000100100;  //0024
        RN[7]  = 16'b0000000011111111;  //00FF
        RN[8]  = 16'b1010101010101010;  //AAAA
        RN[9]  = 0;						//0000
        RN[10] = 0;						//0000
        RN[11] = 0;						//0000
        RN[12] = 16'b1111111111111111;  //FFFF
        RN[13] = 16'b0000000000000010;  //0002
        RN[14] = 16'b0000000000000000;  //0000
        RN[15] = 16'b0000000000000000;  //0000
    end
    
    //Write data on posedge of clock if regWrite = 1
    always@(posedge clk) begin
        
        if( regWrite == 1'b1 ) begin
            RN[whichRegToWriteTo] = writeData;
        end
        
    end
    
    //Set the output based on regre
    assign dataA = RN[selectA];
    assign dataB = RN[selectB];
    
    
endmodule