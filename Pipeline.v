module Pipeline( clk );
    
    input clk;
    
    wire [15:0] noOpCommand;
    assign noOpCommand = 16'b1110_1111_1111_1111;
    
    
    
    //Used to control if we're flushing the pipeline or not.
    //Flush signals for branching
    wire flushOutIDEX, flushOutIFID, resetPC;
    
    ///////////////////
    //Program Counter//
    ///////////////////
    reg [15:0] programCounter;
    
    ////////////////////////////////////////////////
    //IF/ID (Instruction Fetch/Instruction Decode)//
    ////////////////////////////////////////////////
    reg [15:0] fetchDecode; //Result from the instruction memory
    
    
    
    //////////////////////////////////////////////
    //ID/EX (Instruction Decode/Execute regiser)//
    //////////////////////////////////////////////
    // [15:0]  REG A / OP 1
    // [31:16] REG B / OP 2
    // [47:32] Sign extended immedate
    // [51:48] Write back destination register
    // [55:52] Op Code
    // [59:56] ALU function code
    // [63:60] Selected REG A ID
    // [67:64] Selected Reg B ID
    //////////////////////////////////////////////
    reg [67:0] decodeExecute;
    
    
    
    ///////////////////////////
    //EX/MEM (Execute/Memory)//
    ///////////////////////////
    // [15:0]  ALU Result or Memory Address
    // [31:16] Data from OP1/REGA
    // [35:32] WB Register Destination
    // [39:36] Op Code
    ///////////////////////////
    reg [39:0] executeMemory;
    
    
    
    //////////////////////////////
    //MEM/WB (Memory/Write Back)//
    //////////////////////////////
    // [15:0]  Data to write back
    // [19:16] Write Back Destination Register
    // [23:20] Op Code // Used by controller to determine if we're writing back or not
    //////////////////////////////
    reg [23:0] memoryWriteback;
    
    reg isEqualFlag, isLessFlag, isGreaterFlag;
    
    //Iinitilize all registers here:
    initial begin
        programCounter  = 16'b0000000000000000;
        
        //Init registers to noops
        fetchDecode = 16'b1110111111111111;
        
        isEqualFlag = 0;
        isLessFlag = 0; 
        isGreaterFlag = 0;
        
        decodeExecute[51:0]     = 0;
        decodeExecute[55:52]    = 4'b1110;
        decodeExecute[67:56]    = 0;
        
        executeMemory[35:0]     = 0;
        executeMemory[39:36]    = 4'b1110;
        
        
        memoryWriteback[19:0]   = 0;
        memoryWriteback[23:20]  = 4'b1110;
    end
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////MEM/WB <--    STAGED     ---> IF/ID//////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    //SignedAdder( a, b, out );
    
    //LeftShiftOnce( dataIn, dataOut )
    //mux2to1( input    [15:0] d0, d1, input s, output reg [15:0] y );
    
    wire [15:0] shiftedImmedateData;
    LeftShiftOnce leftShifter( decodeExecute[47:32], shiftedImmedateData );
    
    //Need two adders
    wire [15:0] addTwoResult;
    SignedAdder addTwoAdder( programCounter, 16'b0000000000000010, addTwoResult );
    
    wire [15:0] addShiftedResult;
    SignedAdder addShiftedConstantAdder( programCounter, shiftedImmedateData, addShiftedResult );
    
    wire muxDSelectionSignal;
    wire [15:0] dataToPC;
    mux2to1 toPCMux( .d0(addShiftedResult), .d1(addTwoResult), .s(muxDSelectionSignal), .y(dataToPC) );
    
    wire [15:0] instructionMemoryOutput;
    instructionMemory mylifeforaiur( programCounter, instructionMemoryOutput );
    
    wire opOrNoOpSelect;
    wire [15:0] dataToFetchDecode;
    mux2to1 opOrNoOpMux( .d0(noOpCommand), .d1(instructionMemoryOutput), .s(opOrNoOpSelect), .y(dataToFetchDecode));
    
    
    //Debug
    assign opOrNoOpSelect = 1'b1;
    assign muxDSelectionSignal = 1'b1;//Select add 2 for vv
    //Debug
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////MEM/WB <--    END STAGE D     ---> IF/ID//////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////  IF/ID <--    STAGE A    ---> ID/EX  ////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //Registers regBlock(clk, readReg1, readReg2, whichRegToWriteTo, regWrite, writeData, readData1, readData2 );
    //mux4to1(d0, d1, d2, d3, s, y)
    //SignExtend4to16 four2sixteen(din, dout);
    //SignExtend8to16 eight2Sixteen(din, dout);
    //SignExtend12to16 12to16(din, dout);
    
    
    /////////////////////////
    // Sign Extend modules //
    /////////////////////////
    //module mux2to1( input    [15:0] d0, d1, input s, output reg [15:0] y );
    
    
    //Select which data is being sent into the 4to16
    
    wire [15:0] fourTo16SEInput;
    wire [15:0] SE4To16Output;
    wire muxA2Select;
    mux2to1 fourto16selectionMux( .d0({12'b000000000000, fetchDecode[3:0]}), .d1({12'b000000000000, fetchDecode[7:4]}), .s(muxA2Select), .y(fourTo16SEInput) );
    
    SignExtend4To16 se1( fourTo16SEInput[3:0], SE4To16Output );
    
    wire [15:0] SE8To16Output;
    SignExtend8To16 se2( fetchDecode[7:0], SE8To16Output );
    
    wire [15:0] SE12To16Output;
    SignExtend12To16 se3( fetchDecode[11:0], SE12To16Output );
    
    //Sign extended selection mux
    wire [15:0] signExtendedValueOutput;
    wire [1:0] muxA1Select;
    mux4to1 signExtendedSelectMux(.d0(SE4To16Output), .d1(SE8To16Output), .d2(SE12To16Output), .d3(16'b0000000000000000), .s(muxA1Select), .y(signExtendedValueOutput));
    
    
    //////////////////////////////////////
    //          Register Block          //
    //////////////////////////////////////
    
    //Regblock selection Signals.
    wire [3:0] selectA, selectB;
    assign selectA = fetchDecode[11:8];
    assign selectB = fetchDecode[7:4];
    
    //Register Write Controls
    wire[3:0] whichRegToWriteTo;
    assign whichRegToWriteTo =  memoryWriteback[19:16];
    wire regWrite;
    
    
    
    wire[15:0] writeData;
    assign writeData = memoryWriteback[15:0];
    
    //Register output data
    wire[15:0] dataRegA, dataRegB;
    
    Registers regBlock(clk, selectA, selectB, whichRegToWriteTo, regWrite, writeData, dataRegA, dataRegB );
    
   
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////  IF/ID <--    END STAGE A    ---> ID/EX  ////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////  ID/EX <--    STAGE B    ---> EX/MEM  ////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////
    //          ALU FEED A          //
    //////////////////////////////////
    //mux4to1(d0, d1, d2, d3, s, y)
    //module mux2to1( input    [15:0] d0, d1, input s, output reg [15:0] y );
    
    //Controlled by controller
    wire muxB1Select;
    wire [15:0] immedateOrRegAOutput;
    mux2to1 regAOrImmedateMux(.d0(decodeExecute[15:0]), .d1(decodeExecute[47:32]), .s(muxB1Select), .y(immedateOrRegAOutput));
    
    //Controlled by feedback
    wire[15:0] aluAInput;
    wire [1:0] feedbackASelect;
    //Select between the immedate/register, execute memory feedback, or memory writeback feedback.
    mux4to1 feedbackMuxOne(.d0(immedateOrRegAOutput), .d1(executeMemory[15:0]), .d2(memoryWriteback[15:0]), .d3(16'b0000000000000000), .s(feedbackASelect), .y(aluAInput));
    
    //////////////////////////////////
    //          ALU FEED A          //
    //////////////////////////////////
    
    //Controlled by controller
    wire muxB2Select;
    wire [15:0] immedateOrRegBOutput;
    mux2to1 regBOrImmedateMux(.d0(decodeExecute[31:16]), .d1(decodeExecute[47:32]), .s(muxB2Select), .y(immedateOrRegBOutput));
    
    
    //Controlled by feedback
    wire[15:0] aluBInput;
    wire [1:0] feedbackBSelect;
    //Select between the immedate/register, execute memory feedback, or memory writeback feedback.
    mux4to1 feedbackMuxTwo(.d0(immedateOrRegBOutput), .d1(executeMemory[15:0]), .d2(memoryWriteback[15:0]), .d3(16'b0000000000000000), .s(feedbackBSelect), .y(aluBInput));
    
    
    //ALU( input [3:0]  functcode,
	//		input signed [15:0] op1, op2,
	//		output reg cout,
	//		output reg signed [15:0] finalResult, R0
	//	   );
    
    
    wire aluCout;
    wire [15:0] aluResult, aluR0;
    wire [3:0] functionSelectToALUSignal;
    ALU muhALU( .functcode(functionSelectToALUSignal), .op1(aluAInput), .op2(aluBInput), .cout(aluCout), .finalResult(aluResult), .R0(aluR0) );
    
    
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////  ID/EX <--    END STAGE B    ---> EX/MEM  ////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////  EX/MEM <--    STAGE C    ---> MEM/WB  ////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//dataMemory Block
	//address from ex/mem(write data), address, readline, writeline, dataout(readData) 
	wire [15:0] writeDataSTC, outputSTC;
	
	//muxblock Stage C
    wire muxStageCSelect;
	wire [15:0] dataIntoMemWb;
	
	//address, datain, write, read, dataOut
    //Memory(address, writeData, memWrite, memRead, readData);
    wire memoryWrite, memoryRead;
    
    //Debug
    wire [15:0] dataAtMem2;
    
    Memory memStageC(executeMemory[15:0], executeMemory[31:16], memoryWrite, memoryRead, outputSTC[15:0], dataAtMem2);
	 
	//d0, d1, select, data
	mux2to1 muxStageC(.d0(outputSTC[15:0]), .d1( executeMemory[15:0]), .s(muxStageCSelect), .y(dataIntoMemWb[15:0]));
	
	
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////  EX/MEM <--    END STAGE C    ---> MEM/WB  ////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    

	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////       FORWARDING BLOCK BEGIN         /////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
	
	//Mem/WB Reg Input, Ex/Mem Reg Input, 
	
	
	forwarding forwardBlock(.clk(clk),  .memwbReg(memoryWriteback[19:16]), .exmemReg(executeMemory[35:32]), .reqRegA(decodeExecute[63:60]), .reqRegB(decodeExecute[67:64]), .opCode(decodeExecute[55:52]), .functCode(decodeExecute[59:56]), .muxOne(feedbackASelect), .muxTwo(feedbackBSelect));
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////        FORWARDING BLOCK END	      /////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////                OVERALL CONTROLLER                //////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    //module Controller(  clk, opcodeIFID, opcodeIDEX, opcodeEXMEM, opcodeMEMWB, //OpCodes
    //                functionCodeIFID, functionCodeIDEX, //Function Codes
    //                aluFunctionSelect, // ALU Function Selector
    //                muxA1, muxA2, muxB1, muxB2, muxC, muxD1, //Mux Outputs
    //                memoryWrite, memoryRead, regWrite); //Memory Outputs
    
    
    
    Controller controllerBlock( clk, fetchDecode[15:12], decodeExecute[55:52], executeMemory[39:36], memoryWriteback[23:20],
                                fetchDecode[3:0], decodeExecute[59:56],
                                functionSelectToALUSignal,
                                muxA1Select, muxA2Select, muxB1Select, muxB2Select, muxStageCSelect,
                                memoryWrite, memoryRead, regWrite,
                                isEqualFlag, isLessFlag, isGreaterFlag);
    //
    //muxDSelectionSignal
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////             END OVERALL CONTROLLER                //////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    BranchController branchCont( decodeExecute[55:52], aluAInput, aluBInput, flushOutIDEX, flushOutIFID, resetPC);
    wire [15:0] addVa;
    assign addVa = decodeExecute[47:32]*2;
    ///////////////
    //Clock block//
    ///////////////
    always@(posedge clk) begin
        
        //////////////////////////
        //      SET FLAGS       //
        //////////////////////////
        isEqualFlag = (dataRegA == 16'b0000000000000000 && fetchDecode[15:12] == 4'b0110);
        isLessFlag  = (dataRegA[15] == 1'b1 && fetchDecode[15:12] == 4'b0100);
        isGreaterFlag = (dataRegA > 16'b0000000000000000 && fetchDecode[15:12] == 4'b0101);
        
        
        ///////////////////////////////////////
        //          Data into MEM/WB         //
        ///////////////////////////////////////
		memoryWriteback[15:0]   = dataIntoMemWb[15:0] ;
		memoryWriteback[19:16]  = executeMemory[35:32]; 
		memoryWriteback[23:20]  = executeMemory[39:36];
        
        
        ///////////////////////////////////////
        //          Data into EX/MEM         //
        ///////////////////////////////////////
        
        executeMemory[15:0]     = aluResult;                // Grab ALU Result
        executeMemory[31:16]    = decodeExecute[15:0];      // Grab Op1/RegA from ID/EX
        executeMemory[35:32]    = decodeExecute[51:48];     // Grab Write Back From ID/EX
        executeMemory[39:36]    = decodeExecute[55:52];     // Grab OpCode from ID/EX
        
        //////////////////////////////////////
        //          Data into ID/EX         //
        //////////////////////////////////////
        
        if( flushOutIDEX == 1'b1 ) begin    // If we need to flush out this register, set it to a no-op.
            decodeExecute[51:0]     = 0;                        // Set the data to zero
            decodeExecute[55:52]    = 4'b1110;                  // If it's a flush set it to a no-op
            decodeExecute[67:56]    = 0;                        // Set this data to zero.
        end else begin  //Otherwise normal operation
            decodeExecute[15:0]     = dataRegA;                 // Data from RegB/OP1
            decodeExecute[31:16]    = dataRegB;                 // Data from RegA/OP2
            decodeExecute[47:32]    = signExtendedValueOutput;  // Immedate sign extended value
            decodeExecute[51:48]    = fetchDecode[11:8];        // Grab writeback destination from IF/ID
            decodeExecute[55:52]    = fetchDecode[15:12];       // Grab OP code from IF/ID
            decodeExecute[59:56]    = fetchDecode[3:0];         // Grab function code from IF/ID for ALU
            decodeExecute[63:60]    = fetchDecode[11:8];        // REGA / OP1 ID
            decodeExecute[67:64]    = fetchDecode[7:4];         // REGB / OP2 ID
        end
        
        
        
        //////////////////////////////////////
        //          Data into IF/ID         //
        //////////////////////////////////////
        
        
        if( resetPC == 1'b1 ) begin
            programCounter = programCounter - 2 + addVa;
        end else begin
            programCounter = dataToPC;
        end
        //programCounter = dataToPC;
        if( flushOutIFID == 1'b1 ) begin
            fetchDecode = noOpCommand;
        end else begin
            fetchDecode = dataToFetchDecode;
        end
        
       
        
        
        
        
        
        
    end
    
    
endmodule