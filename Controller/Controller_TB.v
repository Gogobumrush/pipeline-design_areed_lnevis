module Controller_TB();
    
    reg clk;
    reg[3:0] opcodeIFID, opcodeIDEX, opcodeEXMEM, opcodeMEMWB, functionCodeIFID, functionCodeIDEX;
    wire [3:0] aluFunctionSelect;
    wire [1:0] muxA1;
    wire muxA2;
    wire muxB1, muxB2;
    wire muxC;
    wire muxD1;
    wire memoryRead, memoryWrite;
    Controller uut( clk, opcodeIFID, opcodeIDEX, opcodeEXMEM, opcodeMEMWB, //OpCodes
                    functionCodeIFID, functionCodeIDEX, //Function Codes
                    aluFunctionSelect, // ALU Function Selector
                    muxA1, muxA2, muxB1, muxB2, muxC, //Mux Outputs
                    memoryWrite, memoryRead, regWrite,
                    isEqual, isLessThan, isGreaterThan); //Memory Outputs
    
    initial begin
        $dumpfile("wave.vcd");
        $dumpvars(0,Controller_TB);
        //Initilize everything to 
        clk = 0;
        opcodeIFID          = 4'b0000;
        opcodeIDEX          = 4'b0000;
        opcodeEXMEM         = 4'b0000;
        opcodeMEMWB         = 4'b0000;
        functionCodeIFID    = 4'b0000;
        functionCodeIDEX    = 4'b0000;
        #1;
        clk = 1;
        #1;
        clk = 0;
        #1 $finish;
    
    end
    
endmodule