module Controller(  clk, opcodeIFID, opcodeIDEX, opcodeEXMEM, opcodeMEMWB, //OpCodes
                    functionCodeIFID, functionCodeIDEX, //Function Codes
                    aluFunctionSelect, // ALU Function Selector
                    muxA1, muxA2, muxB1, muxB2, muxC, //Mux Outputs
                    memoryWrite, memoryRead, regWrite,
                    isEqual, isLessThan, isGreaterThan); //Memory Outputs
    input clk;
    input [3:0] opcodeIFID, opcodeIDEX, opcodeEXMEM, opcodeMEMWB, functionCodeIFID, functionCodeIDEX;
    
    input isEqual, isLessThan, isGreaterThan;
    
    output reg [3:0] aluFunctionSelect;
    
    ///////////////////////////
    //      Stage A          //
    //      Controls         //
    ///////////////////////////
    
    output reg [1:0] muxA1;
    output reg muxA2;
    output reg regWrite;
    
    ///////////////////////////
    //      Stage B          //
    //      Controls         //
    ///////////////////////////
    
    output reg muxB1, muxB2;
    
    ///////////////////////////
    //      Stage C          //
    //      Controls         //
    ///////////////////////////
    output reg muxC; //Data Select
    output reg memoryWrite, memoryRead; //Memory Outputs
    
    
    ///////////////////////////
    //      Stage D          //
    //      Controls         //
    ///////////////////////////
    
    //output reg muxD1;
    
    
    initial begin
        
        muxA1 = 0;
        muxA2 = 0;
        muxB1 = 0;
        muxB2 = 0;
        muxC  = 0;
        //muxD1 = 1;
        memoryRead = 0;
        memoryWrite = 0;
        aluFunctionSelect = 4'b0000;
        regWrite = 1'b0;
        
    end
    
    always@( clk ) begin
        
        
        ////////////////////////////////////////
        //           STAGE A                  //
        ////////////////////////////////////////
        if( opcodeIFID == 4'b0000 ) begin
        
            //Does the instruction use a 4-bit immedaiate?
            if( functionCodeIFID == 4'b1111 ||  functionCodeIFID == 4'b1110 || functionCodeIFID == 4'b1101 || functionCodeIFID == 4'b1100 || functionCodeIFID == 4'b0001 || functionCodeIFID == 4'b0010) begin
                //No. Both op1 and op2 are registers
                muxA1 = 2'b00; //N/A, Set it anywav.
                muxA2 = 1'b0;  //N/A, set it anyway.
            end else begin
                //Yes. Op2 contains an immedate value, and not a register.
                //Select the 4-to-16 sign extend
                muxA1 = 2'b00;
                //Select data [7:4] from the instruction then sign extend it.
                muxA2 = 1'b1;
            end
            
        end else if ( opcodeIFID == 4'b1000 || opcodeIFID == 4'b1011 ) begin
            //Immedate value is stored in [3:0] of the IF/ID Reg
            muxA2 = 1'b0; // Select [3:0] for sign extend
            muxA1 = 2'b0; // Select 4-to-16 bit sign extend
            
        end else if ( opcodeIFID == 4'b0100 || opcodeIFID == 4'b0101 || opcodeIFID == 4'b0110 ) begin
            muxA2 = 1'b0; // Not used. Set it anyway.
            muxA1 = 2'b01; // Select 8 to 16 sign extend
        end else if ( opcodeIFID == 4'b1100 ) begin
            muxA2 = 1'b0; // Not Used. Set it anyway.
            muxA1 = 2'b10; //Select 12 to 16 sign extend
            //TODO: Implement Jumping
        end else if ( opcodeIFID == 4'b1111 ) begin
            //HALT
            //TODO: Implement Halt
        end
        
        
        ////////////////////////////////////////
        //           STAGE B                  //
        ////////////////////////////////////////
        if ( opcodeIDEX == 4'b0000 ) begin
            
            if( functionCodeIDEX == 4'b1111 ||  functionCodeIDEX == 4'b1110 || functionCodeIDEX == 4'b1101 || functionCodeIDEX == 4'b1100 || functionCodeIDEX == 4'b0001 || functionCodeIDEX == 4'b0010) begin
                //Both inputs are registers. Op1 and Op2 are reg A and reg B.
                //Select registers for MuxB1 and MuxB2
                muxB1 = 1'b0;
                muxB2 = 1'b0;
                //In this case, we can set the ALU function to the associated function
                aluFunctionSelect = functionCodeIDEX;
            end else begin
                //op1/RegA is a register
                //Op2 is an immedate value
                //Shift and rotates
                muxB1 = 1'b0;
                muxB2 = 1'b1;
                //Set the ALU function to the stored function
                aluFunctionSelect = functionCodeIDEX;
            end 
            
        end else if ( opcodeIDEX == 4'b1000 || opcodeIDEX == 4'b1011 ) begin
            
            //Load and store. Memory location is always at op2 + immedate
            muxB1 = 1'b1;   // Select immedate
            muxB2 = 1'b0;   // Select Reg B or op2. Add these to get the memory address
            aluFunctionSelect = 4'b1111;
            
        end else if ( opcodeIDEX == 4'b0100 || opcodeIDEX == 4'b0101 || opcodeIDEX == 4'b0110 ) begin
            
            //If a branch is detected, move the register data forward toward the ALU
            muxB1 = 1'b0;   // Select immedate
            muxB2 = 1'b0;
            
        end else if ( opcodeIDEX == 4'b1100 ) begin
            
            
            
            
            //Jump
            //TODO: Implement Jump
        end
        
        
        ////////////////////////////////////////
        //           STAGE C                  //
        ////////////////////////////////////////
        if( opcodeEXMEM == 4'b0000 ) begin
            //Normal instructions
            //Select data from EX/MEM
            muxC = 1'b1;
            memoryRead = 1'b0;
            memoryWrite = 1'b0;
        end else if( opcodeEXMEM == 4'b1000 ) begin
            //Load. Select data from Memory
            muxC = 1'b0;
            memoryRead = 1'b1;
            memoryWrite = 1'b0;
        end else if( opcodeEXMEM == 4'b1011 ) begin
            //Store. Doesn't matter what data is selected
            muxC = 1'b0; //Not used. Set it anyway.
            memoryRead = 1'b0;
            memoryWrite = 1'b1;
        end else if( opcodeEXMEM == 4'b0100 || opcodeEXMEM == 4'b0101 || opcodeEXMEM == 4'b0110 || opcodeEXMEM == 4'b1100 ) begin
            //TODO: WHat happens if branching enters this location?
            
        end else if ( opcodeEXMEM == 4'b1111 ) begin
        
            //TODO: Implement haulting at stage C
            
        end else begin
            muxC = 1'b1; //Not used. Set it anyway.
            memoryWrite = 1'b0;
        end
        
        ////////////////////////////////////////
        //           STAGE D                  //
        ////////////////////////////////////////
        
        if( opcodeMEMWB == 4'b0000 ) begin
            regWrite = 1'b1;
        end else begin
            regWrite = 1'b0;
        end
        
        if( opcodeIDEX == 4'b0100 || opcodeIDEX == 4'b0101 || opcodeIDEX == 4'b0110 || opcodeIDEX == 4'b1100 ) begin
            //Jumping handeled by a different stage/block.
        end else begin
            
            //muxD1 = 1'b1; // Select Add 2.
            
        end
    end

endmodule