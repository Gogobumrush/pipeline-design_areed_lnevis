module SignExtend4To16( din, dout );
    
    input [3:0] din;
    output [15:0] dout;
    
    assign dout[15] = din[3];
    assign dout[14] = din[3];
    assign dout[13] = din[3];
    assign dout[12] = din[3];
    assign dout[11] = din[3];
    assign dout[10] = din[3];
    assign dout[9]  = din[3];
    assign dout[8]  = din[3];
    assign dout[7]  = din[3];
    assign dout[6]  = din[3];
    assign dout[5]  = din[3];
    assign dout[4]  = din[3];
    assign dout[3:0] = din[3:0];
    
endmodule