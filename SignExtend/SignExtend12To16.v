module SignExtend12To16(din, dout);
    
    input [11:0] din;
    
    output [15:0] dout;
    
    assign dout[15] = din[11];
    assign dout[14] = din[11];
    assign dout[13] = din[11];
    assign dout[12] = din[11];
    assign dout[11:0]= din[11:0];
    
endmodule