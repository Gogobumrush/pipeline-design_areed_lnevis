module SignExtend8To16(din, dout);
    
    input [7:0] din;
    
    output [15:0] dout;
    
    assign dout[15] = din[7];
    assign dout[14] = din[7];
    assign dout[13] = din[7];
    assign dout[12] = din[7];
    assign dout[11] = din[7];
    assign dout[10] = din[7];
    assign dout[9]  = din[7];
    assign dout[8]  = din[7];
    assign dout[7:0]= din[7:0];
    
endmodule