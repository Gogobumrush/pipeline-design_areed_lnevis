
module BranchController( opcodeIDEX, compDataA, compDataB, flushOutIDEX, flushOutIFID, resetPC);

    //We need to determine if the branch was taken in the Execute stage.
    //If it was not taken then ignore this and treat it as a no-op in later stages.
    //If it was taken and we made a mistake then we need to set the IFID, and IDEX registers to noops, and set the program counter to PC=PC + jumpImmediate - 4 (Four offset due to the two instructions incorrectly clocked in.)
    input [3:0] opcodeIDEX;
    input signed [15:0] compDataA;
    input signed [15:0] compDataB; //THis should be R0.
    
    output reg flushOutIDEX, flushOutIFID, resetPC; // Control signals to determine if we flush or not.
    
    initial begin
        flushOutIDEX = 0;
        flushOutIFID = 0;
        resetPC = 0;
    end
    
    always@(*) begin
        if( opcodeIDEX == 4'b0100 ) begin //BRANCH ON LESS THAN
            if( compDataA < 0 ) begin
                flushOutIDEX = 1;
                flushOutIFID = 1;
                resetPC = 1;
            end else begin
                //Otherwise do nothing. We've got the correct instructions;
            end
        end else if ( opcodeIDEX == 4'b0101 ) begin //BRANCH ON GREATER THAN
            if( compDataA > 0 ) begin
                flushOutIDEX = 1;
                flushOutIFID = 1;
                resetPC = 1;
            end else begin
                //Otherwise do nothing. We've got the correct instructions;
            end
        end else if ( opcodeIDEX == 4'b0110 ) begin //BRANCH ON EQUAL
            if( compDataA == compDataB ) begin
                flushOutIDEX = 1;
                flushOutIFID = 1;
                resetPC = 1;
            end else begin
                //Otherwise do nothing. We've got the correct instructions;
            end
        end else if(opcodeIDEX == 4'b1100) begin
            //Regular jump.
            flushOutIDEX = 1;
            flushOutIFID = 1;
            resetPC = 1;
        end else begin
            //All other op codes do nothing.
            flushOutIDEX = 0;
            flushOutIFID = 0;
            resetPC = 0;
        end
    end

endmodule